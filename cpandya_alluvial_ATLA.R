library(tidyverse)
library(appa)
library(ggalluvial)

dataDf <- appa::appa

summaryDf <- dataDf %>% 
       select(book_num,chapter_num,character,character_words) %>% 
       filter(character != 'Scene Description')

summaryDf <- summaryDf %>%
             rowwise() %>%
             mutate(num_words = lengths(gregexpr("\\W+", character_words)) + 1)


# Top 15 characters in the show
# https://www.cbr.com/best-characters-avatar-last-airbender/
top_characters <- c("The Boulder","Huu","Hakoda","Pakku","Bumi","Aang","Azula","Ty Lee","Appa","Sokka","Iroh","Katara","Suki","Zuko","Toph")

plotDf <- summaryDf %>% group_by(book_num,chapter_num,character) %>% summarize(total_words=sum(num_words))
plotDf <- plotDf %>% filter(character %in% top_characters)
plotDf$character <- as.factor(plotDf$character)


ggplot(plotDf,
  aes(x = chapter_num, stratum = character, alluvium = character,y = total_words,fill=character,label=character)) +
  geom_flow() +
  geom_stratum(alpha = .5) +
  geom_text(stat = "stratum", size = 3) +
  theme(legend.position = "none") +
  scale_x_continuous(breaks=seq(1,21,1)) +
  facet_grid(rows=vars(book_num)) +
  labs(title="Character vebosity in ATLA", x ="Chapter #", y = "Total # of Words")
